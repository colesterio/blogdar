---
title: Hallada ardilla con un asombroso parecido a Abraham Lincoln
date: "2015-10-21T09:19:03.284Z"
---

![ardilla-Lincoln](./ardilla.jpg)

En la pequeña localidad de Springfield ha sido hallada una ardilla cuya cara recuerda enormemente a la del presidente Abraham Lincoln. Dedicaremos toda la noche si es necesario a ampliar esta información.
