---
title: ¡Vuelve Frikimalismo FM en su cuarta temporada!
date: "2019-11-18T10:20:03.284Z"
---

Así es, los chicos del podcast sobre frikismo, series, películas, videojuegos y, en definitiva, todo lo que un buen friki que se precie necesita han vuelto en su cuarta temporada.

Esta vez el programa se grabará en la tienda Akira Cómics y podréis verlo en nuestro canal de [Youtube](https://www.youtube.com/channel/UCQcdhYPo8cVtfFnr_RF9CWw)

Podéis seguirnos en nuestro [Twitter](https://www.twitter.com/frikimalismofm)

![Colaboradores](./frikimalismofm.jpg)
