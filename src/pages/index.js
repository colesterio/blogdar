import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Button from "../components/button"

class IndexPage extends React.Component {
  render() {
    const siteTitle = "Actividad 1 DAR - Fernando Hidalgo Paredes"

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title="Home"
          keywords={[`blog`, `gatsby`, `javascript`, `react`]}
        />
        <img style={{ margin: 0 }} src="./GatsbyScene.svg" alt="Gatsby Scene" />
        <h1>
          Actividad 1 DAR{" "}
          <span role="img" aria-label="wave emoji">
            👋
          </span>
        </h1>
        <h3>Crea tu blog con un generador de sitios estáticos.</h3>
        <p>
          En esta actividad se ha empleado Gatsby para generar una página estática y posteriormente se ha subido el código a Gitlab y desplegado en Netlify. Puede accederse al blog en el enlace inferior.
	</p>
	<p>
	Autor: Fernando Hidalgo Paredes.
        </p>
        <Link to="/blog/">
          <Button marginTop="35px">Visitar el blog</Button>
        </Link>
      </Layout>
    )
  }
}

export default IndexPage
